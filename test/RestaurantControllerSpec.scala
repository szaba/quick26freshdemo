/**
 * Created with IntelliJ IDEA.
 * User: X.Y.Fan
 * Date: 14-1-20
 * Time: 下午3:22
 * To change this template use File | Settings | File Templates.
 */
import controllers._
import play.api.mvc._
import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._

import play.api.test._
import play.api.test.Helpers._


@RunWith(classOf[JUnitRunner])
object RestaurantControllerSpec extends PlaySpecification with Results {
  class TestController() extends Controller with RestaurantController

  "Page#index" should {
    "should be valid" in {
      val controller = new TestController()
      val result = controller.index().apply( FakeRequest())
      status(result) must equalTo(OK)
      contentAsString(result) must contain("RestaurantController")
    }
  }


}
