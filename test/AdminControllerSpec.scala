/**
 * Created with IntelliJ IDEA.
 * User: X.Y.Fan
 * Date: 14-2-3
 * Time: 下午5:56
 * To change this template use File | Settings | File Templates.
 */
import controllers._
import play.api.mvc._
import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._


import play.api.test._
import play.api.test.Helpers._

//testing with Jenkins integration
@RunWith(classOf[JUnitRunner])
object AdminControllerSpec extends PlaySpecification with Results {
  class TestController() extends Controller with AdminController

  "Page#index" should {
    "should be valid" in {
      val controller = new TestController()
      val result = controller.index().apply( FakeRequest())
      status(result) must equalTo(OK)
      contentAsString(result) must contain("AdminController")
    }
  }


}