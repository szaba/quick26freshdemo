/**
 * Created with IntelliJ IDEA.
 * User: X.Y.Fan
 * Date: 13-11-11
 * Time: 下午2:40
 * To change this template use File | Settings | File Templates.
 */
(function () {
    var module = angular.module("angular-google-maps-example", ["google-maps"]);
}());

function MapController ($scope, $timeout, $log) {

    // Enable the new Google Maps visuals until it gets enabled by default.
    // See http://googlegeodevelopers.blogspot.ca/2013/05/a-fresh-new-look-for-maps-api-for-all.html
    google.maps.visualRefresh = true;

    angular.extend($scope, {

        position: {
            coords: {
                latitude: 45,
                longitude: -73
            }
        },

        /** the initial center of the map */
        centerProperty: {
            latitude: 41.8819,
            longitude: -87.6278
        },

        /** the initial zoom level of the map */
        zoomProperty: 14,

        /** list of markers to put in the map */
        markersProperty: [ {
            latitude: 41.8819,
            longitude: -87.6278
        }],

        // These 2 properties will be set when clicking on the map
        clickedLatitudeProperty: null,
        clickedLongitudeProperty: null,

        eventsProperty: {
            click: function (mapModel, eventName, originalEventArgs) {
                // 'this' is the directive's scope
                $log.log("user defined event on map directive with scope", this);
                $log.log("user defined event: " + eventName, mapModel, originalEventArgs);
            }
        }
    });
}