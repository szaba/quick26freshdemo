/**
 * Created with IntelliJ IDEA.
 * User: danielcoughlin
 * Date: 1/27/14
 * Time: 2:00 PM
 * To change this template use File | Settings | File Templates.
 */

var app = angular.module("app", ['tablemap','ngRoute']);

app.config(function($routeProvider) {
    $routeProvider.when('/AdminRemove',
        {
            templateUrl: "adminpartials/AdminRemove.html",
            controller: "AdminCtrl"
        }
    )
    $routeProvider.when('/AdminModify',
        {
            templateUrl: "adminpartials/AdminModify.html",
            controller: "AdminCtrl"
        }
    )
    $routeProvider.when('/AdminAdd',
        {
            templateUrl: "adminpartials/AdminAdd.html",
            controller: "AdminCtrl"
        }
    )
    $routeProvider.when('/testingAdmin',
        {
            templateUrl: "adminpartials/admintester.html",
            controller: "AdminCtrl"
        }
    )


})



app.controller("AdminCtrl", function AdminCtrl($scope, $http, $log) {

    var globalRest = [];
    var menuToAdd = [];

    $scope.restName = function (restaurant) {
        $http.get('searchByRestaurant/' + "Subway" + '/' + "0").success(function (data) {
        //$http.get('searchAPIs/60606/subway/200 ').success(function (data) {

        $scope.restaurantAPI = data;


    });
    }

    $scope.addToDatabase = function(restaurantAdd){

//        console.log(restaurantAdd)

        var toPersist =
            {
                "api_id": "",
                "name": restaurantAdd.name,
                "menu": restaurantAdd.menu

            };

        if(restaurantAdd.checked===undefined)
            restaurantAdd.checked=true;
        else
            restaurantAdd.checked=!restaurantAdd.checked;

        if(restaurantAdd.checked===true && globalRest.indexOf(toPersist)===-1){
            var myJsonString = JSON.stringify(toPersist);
            //globalRest.push(myJsonString);
            $http.post('/restaurant/', toPersist).success(function (data) {
                console.log(data);
            })


        }
        if(restaurantAdd.checked==false && globalRest.indexOf(toPersist) > -1)
            globalRest.splice(globalRest.indexOf(toPersist));
        console.log(globalRest) ;
        console.log(toPersist);

    }

    $scope.utilityAddDB = function(restaurantAdd){
        var toPersist =
        {
            "api_id": "",
            "name": restaurantAdd.name,
            "menu": restaurantAdd.menu

        };

        var myJsonString = JSON.stringify(toPersist);
        //globalRest.push(myJsonString);
        $http.post('/restaurant/', toPersist).success(function (data) {
            console.log(data);
        })



    }
    $scope.setSelected = function() {
        $scope.selected = this.restaurant.name;
        console.log($scope.selected);
    }

    $scope.getCoordinates = function (){
        $http.get("searchLocation/60606/subway" ).success(function (coordinates){

            $scope.coordLoc = coordinates;

        })


    }
    $scope.searchForRestaurants = function (input) {

        $http.get("/searchByRestaurant/"+input.name+"/0").success(function (restReturn) {
            var toDisplay={
                name: input.name,
                menu: restReturn
            } ;

            $scope.display = toDisplay;

                console.log(toDisplay);
            });

          //console.log(input);



    }
    $scope.open = function() {
        $scope.selection = this.rests.name;
        console.log($scope.selection);

    }

    $scope.modalShown = false;
    $scope.toggleModal = function(input) {
        $scope.modalShown = !$scope.modalShown;
        $scope.selectRest = input;
    };

    $scope.listRestaurants = function () {

        $http.get("/searchOld?location=41.883322,-87.633216&calories=10000").success(function (restReturn) {
            var toDisplay={
                menu: restReturn
            } ;

            $scope.display = toDisplay;

            console.log(toDisplay);
        });

        //console.log(input);



    }

    $scope.editRows = function (restaurantName, status) {

        var sendArray = { "api_id": "",
            "name": restaurantName.name,
            "menu": menuToAdd
    }

        if (status === 'Add') {

            menuToAdd.push({restaurant_name: restaurantName.name,
                restaurant_id: "",
                name: "",
                calories: "",
                sodium: "",
                fat: "",
                protein: "",
                carbs: "",
                fiber: "",
                category: "",
                group: ""
            })

        }
        else if(status === 'RemoveItem'){
            var index=menuToAdd.indexOf(restaurantName)
            menuToAdd.splice(index,1);
            console.log("Looking to remove, ehh?" + index)


        }
        else if(status === 'AddAPI'){
            menuToAdd = restaurantName.menu;


        }
        else if(status=== 'RemoveAPI') {
            //console.log("HERE!")
            var index=menuToAdd.indexOf(restaurantName)
            console.log(index);
            menuToAdd.splice(index,1);

        }
        console.log(menuToAdd)
        $scope.toScreen = menuToAdd
        $scope.sendToBack = sendArray;
    }


})

app.directive('modalDialog', function() {
    return {
        restrict: 'E',
        scope: {
            show: '='
        },
        replace: true, // Replace with the template below
        transclude: true, // we want to insert custom content inside the directive
        link: function(scope, element, attrs) {
            scope.dialogStyle = {};
            if (attrs.width)
                scope.dialogStyle.width = attrs.width;
            if (attrs.height)
                scope.dialogStyle.height = attrs.height;
            scope.hideModal = function() {
                scope.show = false;
            };
        },
        template: "<div class='ng-modal' ng-show='show'><div class='ng-modal-overlay' ng-click='hideModal()'></div><div class='ng-modal-dialog' ng-style='dialogStyle'><div class='ng-modal-close' ng-click='hideModal()'>X</div><div class='ng-modal-dialog-content' ng-transclude></div></div></div>"
    };
});



var myApp = angular.module('tablemap', ['ngTable']).
    controller('DemoCtrl', function($scope, ngTableParams, $sce, $http) {

        $http.get('/search?location=41.883322,-87.633216&calories=500').success(function (restReturn) {

            $scope.restaurant = restReturn;
            console.log(restReturn)
        })





//        $scope.tableParams = new ngTableParams({
//            page: 1,            // show first page
//            count: 10           // count per page
//        }, {
//            total: $scope.restaurant.length, // length of data
//            getData: function($defer, params) {
//                $defer.resolve($scope.restaurant.slice((params.page() - 1) * params.count(), params.page() * params.count()));
//            }
//        });

        $scope.testRestaurant = function () {
//            $http.get('http://localhost:9000/search?location=41.883322,-87.633216&calories=500').success(function (restReturn) {
            $http.get('https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=41.883322,-87.633216&rankby=distance&minprice=1&maxprice=4&types=restaurant&sensor=false&key=AIzaSyD_V59UOFSBV-QyJIoFNWCQ20MPA0Rs_TM').success(function (restReturn) {
                data= restReturn;
                console.log(restReturn)
            })

            console.log(input)



        }
    });/**
 * Created by danielcoughlin on 2/10/14.
 */



