/*global define */

'use strict';

define(function () {

    /* Controllers */

    var controllers = {};

    controllers.SandboxDemoCtrl = function SandboxDemoCtrl($scope) {
        $scope.max = 200;

        $scope.random = function () {
            var value = Math.floor((Math.random() * 100) + 1);
            var type;

            if (value < 25) {
                type = 'success';
            } else if (value < 50) {
                type = 'info';
            } else if (value < 75) {
                type = 'warning';
            } else {
                type = 'danger';
            }

            $scope.showWarning = (type === 'danger' || type === 'warning');

            $scope.dynamic = value;
            $scope.type = type;
        };
        $scope.random();

        $scope.randomStacked = function () {
            $scope.stacked = [];
            var types = ['success', 'info', 'warning', 'danger'];

            for (var i = 0, n = Math.floor((Math.random() * 4) + 1); i < n; i++) {
                var index = Math.floor((Math.random() * 4));
                $scope.stacked.push({
                    value: Math.floor((Math.random() * 30) + 1),
                    type: types[index]
                });
            }
        };
        $scope.randomStacked();


    };
    controllers.SandboxDemoCtrl.$inject = ['$scope'];

    controllers.StartController = function StartController($scope, $window, $modal, $http) {
        angular.extend($scope, {
            center: {
                latitude: 0,
                longitude: 0
            },
            markers: [], // an array of markers,
            zoom: 16 // the zoom level
        });

        $scope.showResults = false;
        $scope.macros = [
            {name: 'Balanced', carb: 40, protein: 30, fat: 30},
            {name: 'High Protein', carb: 30, protein: 50, fat: 20},
            {name: 'Low Carb', carb: 25, protein: 40, fat: 35},
            {name: 'Low Fat', carb: 45, protein: 40, fat: 15}
        ];
        $scope.selectedMacro = $scope.macros[0];

        $scope.results = [];

        $scope.supportsGeo = $window.navigator;
        $scope.position = {};

        var initLocation = function () {
            window.navigator.geolocation.getCurrentPosition(function (position) {
                $scope.$apply(function () {
                    $scope.position = position;
                    $scope.center = {
                        latitude: $scope.position.coords.latitude,
                        longitude: $scope.position.coords.longitude
                    };

                });
            }, function (error) {
                alert(error);
            });
        };

        $scope.open = function () {

            var modalInstance = $modal.open({
                scope: $scope,
                backdrop: 'static',
                keyboard: false,
                templateUrl: 'partials/splashModal.html',
                controller: controllers.SplashModalCtrl,
                windowClass: 'splashModal',
                resolve: {
                    items: function () {
                        return $scope.macros;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
                searchForRestaurants($http, $scope, selectedItem);

            });
        };

        $scope.openMarker = function (restaurant) {

            angular.forEach($scope.markers, function (marker) {
                if (marker.restaurant.address == restaurant.address) {
                    marker.onClicked();
                } else {
                    marker.closeClick();
                }
            });
        }

        $scope.openMenu = function (restaurant) {

            var modalInstance = $modal.open({
                scope: $scope,
                backdrop: 'none',
                keyboard: true,
                templateUrl: 'partials/menuModal.html',
                controller: controllers.MenuModalCtrl,
                windowClass: 'menuModal',
                resolve: {
                    items: function () {
                        return restaurant.menu;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
                searchForRestaurants($http, $scope, selectedItem);

            });
        };

        initLocation();
        $scope.open();
    };
    controllers.StartController.$inject = ['$scope', '$window', '$modal', '$http'];

    controllers.SplashModalCtrl = function ($scope, $modalInstance, items) {

        $scope.max = 100;
        $scope.items = items;
        $scope.item = items[0];
        $scope.userSearch = {};
        $scope.userSearch.calories = 600;

        $scope.ok = function () {
            $modalInstance.close($scope.userSearch);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.updateMacros = function (selected) {
            $scope.selectedMacro = selected;

            $scope.stacked = [];
            var types = ['success', 'info', 'warning'];

            $scope.stacked.push({
                value: Math.floor(selected.protein),
                type: types[0],
                name: "protein"
            });

            $scope.stacked.push({
                value: Math.floor(selected.fat),
                type: types[1],
                name: "fat"
            });

            $scope.stacked.push({
                value: Math.floor(selected.carb),
                type: types[2],
                name: "carbs"
            });
        };
        $scope.updateMacros($scope.items[0]);

    };

    controllers.MenuModalCtrl = function ($scope, $modalInstance, items) {

        $scope.items = items;
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.getColor = function (targetMacro, macro, type) {
            var types = ['info', 'warning', 'danger'];

            if (type == "Protein") {
                if (macro >= targetMacro) {
                    return types[0];
                }
                if (macro >= targetMacro * .85) {
                    return types[1];
                }
                return types[2];
            }

            if (macro <= targetMacro) {
                return types[0];
            }
            if (macro <= targetMacro * 1.15) {
                return types[1];
            }
            return types[2];
        };


        angular.forEach($scope.items, function (item) {
            item.bars = [];

            var sum = Math.floor(item.protein + item.fat + item.carbs);
            var pctProtein = Math.floor(item.protein / sum * 100);
            var pctCarbs = Math.floor(item.carbs / sum * 100);
            var pctFat = Math.floor(item.fat / sum * 100);
            var remainder = 100 - (pctProtein + pctCarbs + pctFat);

            var dispC = pctCarbs + remainder;
            var dispP = pctProtein;
            var dispF = pctFat;

            if (dispC + dispP > 60 || dispC + dispF > 60) {
                dispF = Math.floor(30 * pctFat / 100) + 10;
                dispP = Math.floor(30 * pctProtein / 100) + 10;
                dispC = 100 - (dispF + dispP);
            }


            item.bars.push({
                value: dispP,
                type: $scope.getColor($scope.selectedMacro.protein, pctProtein, "Protein"),
                name: "Protein",
                gram: item.protein
            });
            item.bars.push({
                value: dispC,
                type: $scope.getColor($scope.selectedMacro.carb, pctCarbs, "Carbs"),
                name: "Carbs",
                gram: item.carbs
            });
            item.bars.push({
                value: dispF,
                type: $scope.getColor($scope.selectedMacro.fat, pctFat, "Fat"),
                name: "Fat",
                gram: item.fat
            });
        });
    };

    function searchForRestaurants($http, $scope, searchCriteria) {
        $http.get('/search?location=' + $scope.position.coords.latitude + ',' + $scope.position.coords.longitude +
                '&radius=1000&calories=' + searchCriteria.calories
            ).success(function (data) {

                onGetRestaurants($scope, data);
            });
    }

    function onGetRestaurants($scope, data) {
        $scope.results = data;

        angular.forEach(data, function (item) {
            var marker = {
                latitude: item.latitude,
                longitude: item.longitude,
                restaurant: item,
                showWindow: false
            };

            $scope.markers.push(marker);

        });

        angular.forEach($scope.markers, function (marker) {
            marker.onClicked = function () {
                marker.showWindow = true;
                //$scope.openMenu(marker.restaurant)
                $scope.$apply();
            };

            marker.closeClick = function () {
                marker.showWindow = false;
                $scope.$apply();
            };
            marker.openMenu = function () {
                $scope.openMenu(marker.restaurant);
                $scope.$apply();
            };

        });

        $scope.showResults = true;
    }

    return controllers;

})
;