/*global define, angular */

'use strict';

require.config({
    paths: {
        async: "./async"
    }
});

define('angular', ["webjars!angular-locale_en-us.js"], function () {
    return angular;
});

require(['angular', './controllers', './directives', './filters', './services', './ui-bootstrap-tpls-0.10.0','./map-directive'],
    function (angular, controllers) {

// Declare app level module which depends on filters, and services
        angular.module('qfApp', ['qfApp.filters', 'qfApp.services', 'qfApp.directives', 'ui.bootstrap','google-maps']).
            config(['$routeProvider', function ($routeProvider) {
                $routeProvider.when('/users', {templateUrl: 'partials/users.html', controller: controllers.UserCtrl});
                $routeProvider.when('/sandbox', {templateUrl: 'partials/sandbox.html', controller: controllers.SandboxDemoCtrl});
                $routeProvider.when('/start', {templateUrl: 'partials/start.html', controller: controllers.StartController});

                //$routeProvider.when('/start', {templateUrl: 'partials/start.html', controller: controllers.SearchController});

                 $routeProvider.otherwise({redirectTo: '/start'});
            }]);

        angular.bootstrap(document, ['qfApp']);

    });
