package models

/**
 * Created By: patrickl on 10/28/13
 */
object JsonFormats {

  import play.api.libs.json.Json

  // Generates Writes and Reads for Feed and User thanks to Json Macros
  //implicit val menuFormat = Json.format[Menu]
  implicit val restaurantFormat = Json.format[Restaurant]
  implicit val feedFormat = Json.format[Feed]
  implicit val userFormat = Json.format[User]
}