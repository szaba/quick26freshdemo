package models


case class MenuItem(
                     id: Long,
                     //restaurant: Restaurant,
                     name: String,
                     size: String,
                     calories: Int,
                     fat: Int,
                     protein: Int,
                     carbs: Int
                     )
