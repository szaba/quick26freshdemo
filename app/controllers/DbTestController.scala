package controllers

import play.api.mvc._


trait DbTestController {
  this: Controller =>

  def index() = Action {
    Ok("DbTestController")
  }
}
object DbTestController extends Controller {

  import models.JsonFormats._

}