package controllers


import play.api._
import play.api.mvc._
import play.api.libs.json._
import play.api.libs.ws._
import play.api.libs.concurrent.Execution.Implicits._
import scala.concurrent.Await
import java.net.URLEncoder
import akka.util.Timeout
import scala.concurrent.Future
import scala.concurrent.duration._


trait SearchController {
  this: Controller =>

  def index() = Action {
    Ok("SearchController")
  }
}
object SearchController extends Controller {

  //This our old search function that used Nutritionix to get nutritioal menu information
  //It takes location as "lat,long" and max calories as input
  //Example: http://localhost:9000/search?location=41.883322,-87.633216&calories=500
  def search(location: String, calories: String) = Action {
    val url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + location + "&rankby=distance&minprice=1&maxprice=4&types=restaurant&sensor=false&key=AIzaSyD_V59UOFSBV-QyJIoFNWCQ20MPA0Rs_TM"
    val url2 = "https://api.nutritionix.com/v1_1/search"

    def buildResponseJson(name:JsValue, address:JsValue, lat:JsValue, long:JsValue, price:JsValue, menuItems: Seq[JsValue], calResults:Seq[JsValue], sodiumResults:Seq[JsValue], fatResults:Seq[JsValue], proteinResults:Seq[JsValue], carbResults:Seq[JsValue], fiberResults:Seq[JsValue]):JsValue={
      var menu:JsArray = new JsArray()
      val category:JsArray = new JsArray()
      for (i <- 0 until menuItems.length) {
        val j=Json.obj(
          "name" -> menuItems(i),
          "portion" -> Json.toJson(""),
          "calories" -> calResults(i),
          "sodium" -> sodiumResults(i),
          "fat" -> fatResults(i),
          "protein" -> proteinResults(i),
          "carbs" -> carbResults(i),
          "fiber" -> fiberResults(i),
          "category" -> category,
          "group" -> Json.toJson("")
        )
        menu :+= j
      }
      val responseJson = Json.obj(
        "name" -> name,
        "address" -> address,
        "latitude" -> lat,
        "longitude" -> long,
        "price_level" -> price,
        "menu"-> menu
      )
      //Logger.debug(responseJson.toString())
      return responseJson
    }

    Async {
      def restaurantChoices(result:play.api.libs.ws.Response):JsValue = {
        var buildJsonArray:JsArray = new JsArray()
        val address = result.json \ "results" \\ "vicinity"
        val name = result.json \ "results" \\ "name"
        val lat = result.json \ "results" \\ "lat"
        val long = result.json \ "results" \\ "lng"
        val price = result.json \ "results" \\ "price_level"
        var limit = address.length

        if (limit > 5)  {
          limit = 5;
        }
        for (j <- 0 until limit) {
          val search = Json.obj(
            "appId" -> "75cd310f",
            "appKey" -> "7e80d087e85db460b6c6a5a119fe37f8",
            "fields" -> Json.arr("item_name","brand_name","brand_id","item_id","nf_calories", "nf_sodium", "nf_total_fat", "nf_protein", "nf_total_carbohydrate", "nf_dietary_fiber"),
            "offset"-> 0,
            "limit" -> 50,
            "queries" -> Json.obj(
              "brand_name" -> name(j)
            ),
            "filters" -> Json.obj(
              "item_type" -> 1,
              "nf_calories" -> Json.obj (
                "from" ->200,
                "to" -> calories
              )
            )
          )

          val calorieResponse= WS.url(url2).post(search)
          val calResponse=Await.result(calorieResponse, Duration(50000, "millis"))
          val menuItems = calResponse.json  \ "hits" \\ "item_name"
          val calResult =  calResponse.json  \ "hits" \\ "nf_calories"
          val sodiumResult =  calResponse.json  \ "hits" \\ "nf_sodium"
          val fatResult =  calResponse.json  \ "hits" \\ "nf_total_fat"
          val proteinResult =  calResponse.json  \ "hits" \\ "nf_protein"
          val carbResult =  calResponse.json  \ "hits" \\ "nf_total_carbohydrate"
          val fiberResult =  calResponse.json  \ "hits" \\ "nf_dietary_fiber"

          //Logger.debug("menuItems")
          //Logger.debug(menuItems.length.toString())
          //Logger.debug(fatResult.length.toString())

          if (menuItems.length > 0 && (menuItems.length == fatResult.length) && (menuItems.length == sodiumResult.length) && (menuItems.length == proteinResult.length) && (menuItems.length == carbResult.length) && (menuItems.length == fiberResult.length)) {
            buildJsonArray :+= buildResponseJson(name(j), address(j), lat(j), long(j), price(j), menuItems, calResult, sodiumResult, fatResult, proteinResult, carbResult, fiberResult)
          }
        }
        return buildJsonArray
      }


      val locationResponse= WS.url(url).get()
      locationResponse.map {
        response =>
          Ok(restaurantChoices(response))
      }
    }
  }

  def searchDatabase(location: String, calories: String) = Action {
    //Setting up urls for API requests
    val url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + location + "&rankby=distance&minprice=1&maxprice=4&types=restaurant&sensor=false&key=AIzaSyD_V59UOFSBV-QyJIoFNWCQ20MPA0Rs_TM"
    //val url2= "http://localhost:9000/restaurant/"
    val url2="http://quick26fresh.no-ip.org/restaurant/"

    //buildFakeMenu is used to make Json Array with fake menu information for use with restaurants that are not yet in our database
    def buildFakeMenu():JsArray={
      var tempMenu:JsArray = new JsArray()
      val category:JsArray = new JsArray()
      for (i <- 0 until 10) {
        val menuItem=Json.obj(
          "name" -> Json.toJson("menuItem" + i),
          "portion" -> Json.toJson(""),
          "calories" -> (calories.toInt-i),
          "fat" -> (10+i),
          "protein" -> (15+i),
          "carbs" -> (25+i),
          "category" -> category,
          "group" -> Json.toJson("")
        )
        tempMenu :+= menuItem
      }
      return tempMenu
    }

    //buildResponseJson will build Jsons for each restuarant that will be returned as part of a JsonArray
    def buildResponseJson(name:JsValue, address:JsValue, lat:JsValue, long:JsValue, price:JsValue, menuItems:JsValue):JsValue={
      //Pulls out necessary information from our database response
      val itemNames = menuItems \\ "name"
      val cal = menuItems \\ "calories"
      val fat = menuItems \\ "fat"
      val carbs = menuItems \\ "carbs"
      val protein = menuItems \\ "protein"

      var hasMatch = 0
      var menu:JsArray = new JsArray()  //Empty array that will be filled with menu items that match max calories
      for (i <- 0 until itemNames.length) {  //Walk through each menu item returned for restaurant
        var calTemp = cal(i).toString()
        if (cal(i).toString.head == '"') {  //Some restaurants put calores in quotes, need to remove quotes
          calTemp=cal(i).toString().substring(1).dropRight(1)
        }
        if (calTemp.toDouble <= calories.toDouble) {   //If calories is under max calories add menu item to menu array
          hasMatch = 1
          val cat:JsArray = new JsArray()
          val j=Json.obj(
            "name" -> itemNames(i),
            "portion" -> Json.toJson(""),
            "calories" -> cal(i),
            "fat" -> fat(i),
            "protein" -> protein(i),
            "carbs" -> carbs(i),
            "category" -> cat,
            "group" -> Json.toJson("")
          )
          menu :+= j
        }
      }
      if (hasMatch == 0) {   //If restaurant has no matches, build fake menu for now
        menu=buildFakeMenu()
      }
      val responseJson = Json.obj(   //Create the json for the restaurant
        "name" -> name,
        "address" -> address,
        "latitude" -> lat,
        "longitude" -> long,
        "price_level" -> price,
        "menu"-> menu
      )
      return responseJson   //return json
    }

    //This is the Asyc part that calls that APIs
    Async {
      //restaurantChoices take response from GoogleAPI and sends requests to our database
      def restaurantChoices(result:play.api.libs.ws.Response):JsValue = {
        var buildJsonArray:JsArray = new JsArray()

        //All required information is pulled from the GoogleAPI result
        val address = result.json \ "results" \\ "vicinity"
        val name = result.json \ "results" \\ "name"
        val lat = result.json \ "results" \\ "lat"
        val long = result.json \ "results" \\ "lng"
        val price = result.json \ "results" \\ "price_level"

        for (j <- 0 until address.length) {  //for each restaurant in GoogleAPI results
          val restaurantName = name(j).toString().substring(1).dropRight(1) //remove quotes from restaurant name
          val nameEncoded= (URLEncoder.encode(restaurantName, "UTF-8")).replace("+", "%20")  //URL encode restaurant name
          val dbUrl=url2+nameEncoded //build database url
          val menuResponse= WS.url(dbUrl).get()   //send request to database
          val response=Await.result(menuResponse, Duration(50000, "millis"))  //await response from database

          if (response.json.toString() != "[[]]") {  //if we have menu information in database for restuarant
            val menuResponse = response.json  \\ "menu"
            //Call buildReponseJson to build json for restuarant and add to array
            buildJsonArray :+= buildResponseJson(name(j), address(j), lat(j), long(j), price(j), menuResponse(0))
          }
          else {
            //If we don't have menu information for restaurant in our database, add build response json with fake menu information
            val testJson=Json.obj(
              "name" -> name(j),
              "address" -> address(j),
              "latitude" -> lat(j),
              "longitude" -> long(j),
              "price_level" -> price(j),
              "menu" -> buildFakeMenu()
            )
            buildJsonArray :+= testJson
          }
        }
        //return the array with all restaurant jsons
        return buildJsonArray
      }

      //Initial call to google API to find restaurants nearby
      val locationResponse= WS.url(url).get()
      locationResponse.map {
        response =>  //send response from google API to the restaurantChoices function
          Ok(restaurantChoices(response))
      }
    }
  }


//A test search function that calls the GoogleAPI but does not call Nutritionix or our databse, just returns fake menu infomration
  def testSearch(location: String, calories: String) = Action {
    Async {
      val url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + location + "&rankby=distance&minprice=1&maxprice=4&types=restaurant&sensor=false&key=AIzaSyD_V59UOFSBV-QyJIoFNWCQ20MPA0Rs_TM"
      WS.url(url).get().map {
        response =>
          val address = response.json \ "results" \\ "vicinity"
          val name = response.json \ "results" \\ "name"
          val lat = response.json \ "results" \\ "lat"
          val long = response.json \ "results" \\ "lng"
          val price = response.json \ "results" \\ "price_level"
          //Logger.debug(price.length.toString())

          var menu:JsArray = new JsArray()
          val category:JsArray = new JsArray()
          for (i <- 0 until 10) {
            val menuItem=Json.obj(
              "name" -> Json.toJson("menuItem" + i),
              "portion" -> Json.toJson(""),
              "calories" -> (calories.toInt+i),
              "sodium" -> (20+i),
              "fat" -> (10+i),
              "protein" -> (15+i),
              "carbs" -> (25+i),
              "fiber" -> (1+i),
              "category" -> category,
              "group" -> Json.toJson("")
            )
            menu :+= menuItem
          }

          var results:JsArray = new JsArray()
          for (i <- 0 until address.length) {
            val j=Json.obj(
              "name" -> name(i),
              "address" -> address(i),
              "latitude" -> lat(i),
              "longitude" -> long(i),
              "price_level" -> price(i),
              "menu" -> menu
            )
            results :+= j
          }
          Ok(results)
      }
    }
  }


  //Search by restaurant name to get nutritional information for database
  // Start offset at zero, increase by 50 each time
  def searchByRestaurant(name: String, offset: String) = Action {
    Async {
      val url = "https://api.nutritionix.com/v1_1/search"
      val search = Json.obj(
        "appId" -> "75cd310f",
        "appKey" -> "7e80d087e85db460b6c6a5a119fe37f8",
        "fields" -> Json.arr("item_name","brand_name","brand_id","item_id","nf_calories", "nf_sodium", "nf_total_fat", "nf_protein", "nf_total_carbohydrate", "nf_dietary_fiber"),
        "offset"-> offset,
        "limit" -> 50,
        "queries" -> Json.obj(
          "brand_name" -> name
        ),
        "filters" -> Json.obj(
          "item_type" -> 1
        )
      )
      WS.url(url).post(search).map {
        response =>
          val restaurantName = response.json  \ "hits" \\ "brand_name"
          val restaurantId = response.json  \ "hits" \\ "brand_id"
          val menuItems = response.json  \ "hits" \\ "item_name"
          val calResult =  response.json  \ "hits" \\ "nf_calories"
          val sodiumResult =  response.json  \ "hits" \\ "nf_sodium"
          val fatResult =  response.json  \ "hits" \\ "nf_total_fat"
          val proteinResult =  response.json  \ "hits" \\ "nf_protein"
          val carbResult =  response.json  \ "hits" \\ "nf_total_carbohydrate"
          val fiberResult =  response.json  \ "hits" \\ "nf_dietary_fiber"
          var menu:JsArray = new JsArray()
          for (i <- 0 until menuItems.length) {
            val j=Json.obj(
              "restaurant_name" -> restaurantName(i),
              "restaurant_id" -> restaurantId(i),
              "name" -> menuItems(i),
              "calories" -> calResult(i),
              "sodium" -> sodiumResult(i),
              "fat" -> fatResult(i),
              "protein" -> proteinResult(i),
              "carbs" -> carbResult(i),
              "fiber" -> fiberResult(i)
            )
            menu +:= j
          }
          Ok(menu)
      }
    }
  }

}
