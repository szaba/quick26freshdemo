package controllers

import play.api._
import play.api.mvc._
import reactivemongo.core.commands.LastError
import play.api.libs.json.JsArray
import play.modules.reactivemongo.json.collection.JSONCollection
import play.api.libs.json.JsObject

//import play.api.libs.concurrent.Execution.Implicits.defaultContext

import play.api.libs.json._
import scala.concurrent.Future

// Reactive Mongo imports

import reactivemongo.api._

// Reactive Mongo plugin, including the JSON-specialized collection

import play.modules.reactivemongo.MongoController
import play.modules.reactivemongo.json.collection.JSONCollection

/*
 * Example using ReactiveMongo + Play JSON library,
 * using case classes that can be turned into Json using Reads and Writes.
 *
 * Instead of using the default Collection implementation (which interacts with
 * BSON structures + BSONReader/BSONWriter), we use a specialized
 * implementation that works with JsObject + Reads/Writes.
 *
 * Of course, you can still use the default Collection implementation
 * (BSONCollection.) See ReactiveMongo examples to learn how to use it.
 */
trait RestaurantController {
  this: Controller =>

  def index() = Action {
    Ok("RestaurantController")
  }
}

object RestaurantController extends Controller with MongoController {
  /*
   * Get a JSONCollection (a Collection implementation that is designed to work
   * with JsObject, Reads and Writes.)
   * Note that the `collection` is not a `val`, but a `def`. We do _not_ store
   * the collection reference to avoid potential problems in development with
   * Play hot-reloading.
   */
  def collection: JSONCollection = db.collection[JSONCollection]("restaurants")

  // ------------------------------------------ //
  // Using case classes + Json Writes and Reads //
  // ------------------------------------------ //

  import play.api.data.Form
  import models._
  import models.JsonFormats._


  def upsert = Action(parse.json) {
    request =>
      Async {
        import play.api.libs.json.Reads._
        /*
         * request.body is a JsValue.
         * There is an implicit Writes that turns this JsValue as a JsObject,
         * so you can call insert() with this JsValue.
         * (insert() takes a JsObject as parameter, or anything that can be
         * turned into a JsObject using a Writes.)
         */
        //        val transformer: Reads[JsObject] =
        //          Reads.jsPickBranch[JsString](__ \ "restaurant")

        collection.save(request.body).map {
          lastError =>
            Logger.debug(s"Successfully inserted with LastError: $lastError")
            Created
        }

      }
  }

  def listRestaurants() = Action {
    Async {

      val cursor: Cursor[JsObject] = collection.
        find(Json.obj()).
        // sort them by creation date
        sort(Json.obj("created" -> -1)).
        // perform the query and get a cursor of JsObject
        cursor[JsObject]

      // gather all the JsObjects in a list
      val futureUsersList: Future[List[JsObject]] = cursor.toList()

      // everything's ok! Let's reply with the array
      futureUsersList.map {
        restaurants =>
          Ok(Json.toJson(restaurants))
      }
    }
  }

  def remove(name: String) = Action {
    // let's do our query
    Async {
      collection.remove(Json.obj("name" -> name)).map {
        lastError =>
          Logger.debug(s"Successfully Deleted with LastError: $lastError")
          Ok
      }
    }
  }

  def findByList(names: List[String]): Future[JsArray] = {

    // let's do our query
    val cursor: Cursor[JsObject] = collection.

      //find(Json.obj()).
      find(Json.obj("name" -> Json.obj("$in" -> names))).
      cursor[JsObject]

    // gather all the JsObjects in a list
    val futureRestaurantList: Future[List[JsObject]] = cursor.toList()

    val futurePersonsJsonArray: Future[JsArray] = futureRestaurantList.map {
      restaurant =>
        Json.arr(restaurant)
    }

    return futurePersonsJsonArray
  }

  def findByName(name: String) = Action {
    // let's do our query
    Async {

      // transform the list into a JsArray
      val futurePersonsJsonArray: Future[JsArray] = findByList(List(name))

      // everything's ok! Let's reply with the array
      futurePersonsJsonArray.map {
        restaurants =>
          Ok(restaurants)

      }
    }
  }

  def findByNames(names: List[String]) = Action {
    // let's do our query
    Async {

      val futurePersonsJsonArray: Future[JsArray] = findByList(names)

      // everything's ok! Let's reply with the array
      futurePersonsJsonArray.map {
        restaurants =>
          Ok(restaurants)
      }
    }
  }
}