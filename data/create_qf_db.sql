SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `qf` DEFAULT CHARACTER SET latin1 ;
USE `qf` ;

-- -----------------------------------------------------
-- Table `qf`.`categories`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `qf`.`categories` ;

CREATE  TABLE IF NOT EXISTS `qf`.`categories` (
  `id` INT(11) NOT NULL ,
  `name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `qf`.`restaurants`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `qf`.`restaurants` ;

CREATE  TABLE IF NOT EXISTS `qf`.`restaurants` (
  `id` INT(11) NOT NULL ,
  `name` VARCHAR(100) NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `qf`.`menu_items`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `qf`.`menu_items` ;

CREATE  TABLE IF NOT EXISTS `qf`.`menu_items` (
  `id` INT(11) NOT NULL ,
  `restaurant_id` INT(11) NOT NULL ,
  `name` VARCHAR(45) NOT NULL ,
  `size` VARCHAR(45) NULL DEFAULT NULL ,
  `calories` INT(11) NULL DEFAULT NULL ,
  `fat` INT(11) NULL DEFAULT NULL ,
  `protein` INT(11) NULL DEFAULT NULL ,
  `carbs` INT(11) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `REST_MENU` (`restaurant_id` ASC) ,
  CONSTRAINT `resturant_menu`
    FOREIGN KEY (`restaurant_id` )
    REFERENCES `qf`.`restaurants` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `qf`.`item_categories`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `qf`.`item_categories` ;

CREATE  TABLE IF NOT EXISTS `qf`.`item_categories` (
  `id` INT(11) NOT NULL ,
  `menu_item` INT(11) NOT NULL ,
  `category` INT(11) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `item_fk_idx` (`menu_item` ASC) ,
  INDEX `cat_fk_idx` (`category` ASC) ,
  CONSTRAINT `item_fk`
    FOREIGN KEY (`menu_item` )
    REFERENCES `qf`.`menu_items` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `cat_fk`
    FOREIGN KEY (`category` )
    REFERENCES `qf`.`categories` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

USE `qf` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
